from flask import Flask

app = Flask(__name__)

URL = "http://second-api:5002/"

@app.route('/')
def hello_world():
    try:
        response = requests.get(URL)
        response.raise_for_status()
        return {"data": response.json()}
    except requests.exceptions.RequestException as e:
        raise HTTPException(status_code=500, detail=f"Error: {str(e)}")


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001, debug=True)
